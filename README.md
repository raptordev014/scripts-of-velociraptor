En ce glorieux jour du mardi 29 janvier 2019, moi The Velociraptor place ce projet de scripts libres sous la licence « Fais pas chier ».*

La copie, la reproduction ou la modification est autorisé en indiquant et conservant le nom de l'auteur. Ce projet est Open Source.

Copy, Fork or Modifications are allowed by indicating and keeping author name. This project is Open Source.

The Velociraptor 0.14 (C) 2019.1

All my script are runnable using ./ or system $PATH variable. My scripts DOES NOT contain any virus, malware or spyware methods. If you want to ensure you're safe, use cat [file] commande to see my code script.

Tout mes scripts sont fonctionnels avec le ./ ou la variable systéme $PATH. Mes scripts NE CONTIENNENT aucun virus, malware ou logiciels espions aux méthodes douteuses. Si vous désirez vous assurez qu'aucun virus n'est présent, utiliser la commande cat [fichier] pour voir mes codes.
