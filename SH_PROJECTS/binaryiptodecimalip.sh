#!/usr/bin/env bash
ip1=0
ip2=0
ip3=0
ip4=0
cb1=
cb2=
cb3=
cb4=
printf '
------------------------------
  IP BIN TO IP DEC CONVERTER
------------------------------
This script will convert your binary
ip address to decimal one.

Enter all dot to dot values
(xxxxxxxx.xxxxxxxx.xxxxxxxx.xxxxxxxx)

'
echo "Enter your first dot (________.) : "
read ip1
echo "ibase=2;obase=A;$ip1" | bc > ....cb1
echo "Enter your second dot ($ip1.________): ."
read ip2
echo "ibase=2;obase=A;$ip2" | bc > ....cb2
echo "Enter your third dot ($ip1.$ip2.________): ."
read ip3
echo "ibase=2;obase=A;$ip3" | bc > ....cb3
echo "Enter your fourth ($ip1.$ip2.$ip3.________): ."
read ip4
echo "ibase=2;obase=A;$ip4" | bc > ....cb4
echo "Your binary ip address :"
echo "$ip1.$ip2.$ip3.$ip4"
echo "Translated in Decimal :"
echo "`cat ....cb1`.`cat ....cb2`.`cat ....cb3`.`cat ....cb4`"
echo
echo "Finished."
rm ....cb1 ....cb2 ....cb3 ....cb4