#!/usr/bin/env bash
ip1=0
ip2=0
ip3=0
ip4=0
bc1=
bc2=
bc3=
bc4=
printf '
------------------------------
  IP DEC TO IP BIN CONVERTER
------------------------------
This script will convert your ip address
to binary one.

Enter all dot to dot values (xxx.xxx.xxx.xxx)

'
echo "Enter your first dot	___. : "
read ip1
echo "scale=8;obase=2;$ip1" | bc > ....bc1
echo "Enter your second dot 	$ip1.___.: ."
read ip2
echo "scale=8;obase=2;$ip2" | bc > ....bc2
echo "Enter your third dot 	$ip1.$ip2.___.: ."
read ip3
echo "scale=8;obase=2;$ip3" | bc > ....bc3
echo "Enter your fourth 	$ip1.$ip2.$ip3.___: ."
read ip4
echo "scale=8;obase=2;$ip4" | bc > ....bc4
echo "Your ip address :"
echo "$ip1.$ip2.$ip3.$ip4"
echo "Translated in Binary :"
echo "`cat ....bc1`.`cat ....bc2`.`cat ....bc3`.`cat ....bc4`"
echo
echo "Finished."
rm ....bc1 ....bc2 ....bc3 ....bc4
