#!/usr/bin/env bash
# This script translate decimal read values to binary one.
number=0
printf '
-------------------------------
  BINARY TO DECIMAL CONVERTER  
-------------------------------
This script will translate your
binary number to decimal one.

Using bc command
(need to be present on your system to work)
'
echo -n "Enter your value : "
read number
echo "Converting!"
sleep 1
echo "ibase=2;obase=A;$number" | bc